package bookkeeper.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloAPI {
    @RequestMapping(path = "/api/v1/hello/{name}")
    public String sayHello (@PathVariable("name") String name){

        return "Hello 我是"+ name;
    }
}
