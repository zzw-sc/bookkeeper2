package bookkeeper.controller;

import bookkeeper.mapper.UserInfoMapper;
import bookkeeper.model.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class UserController {
    @Autowired
    UserInfoMapper userInfoMapper;

    @GetMapping("v1/users/{id}")
    UserInfo getUserInfoById(@PathVariable("id") Long id){
        return userInfoMapper.getUserInfoByUserId(id);
    }
}
