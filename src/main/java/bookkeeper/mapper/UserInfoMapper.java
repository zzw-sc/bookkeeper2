package bookkeeper.mapper;

import bookkeeper.model.UserInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;


@Repository
@Mapper
public interface UserInfoMapper {
    @Select("SELECT * FROM ms_userinfo WHERE id = #{id}")
    UserInfo getUserInfoByUserId(Long id);
}
